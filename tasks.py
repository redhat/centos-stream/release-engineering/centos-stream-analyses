#!/usr/bin/python3

import json
import sqlite3
from pathlib import Path

from invoke import task
from rich.console import Console
from rich.table import Table

from util import render_html
from util.config_lists import (
    get_koji_allowlist,
    get_packages_from_json,
    get_zuul_centos_distgit,
)
from util.pkg_names import get_modules_pkgs, get_srpm_names, initialise, load_data
from util.repo_branches import getAllBranches
from util.task_decorators import init_settings, store_result

###############################################################################
###  Main  ####################################################################
###############################################################################


@task
@init_settings("data")
def clean(c, data=None):
    c.run(f"rm -Rf {data}")
    c.run(f"rm -Rf analyses")


@task
@init_settings("data")
@store_result("srpm_names.json")
def srpm_names(c, data=None):
    settings = initialise()
    return get_srpm_names(settings)


@task
@init_settings("data")
@store_result("module_pkg_names.json")
def module_pkg_names(c, data=None):
    settings = initialise()
    return get_modules_pkgs(settings)


@task
@init_settings("data")
@store_result("koji_allowlist_8.json")
def koji_allowlist_8(c, data=None):
    return get_koji_allowlist(8)


@task
@init_settings("data")
@store_result("koji_allowlist_9.json")
def koji_allowlist_9(c, data=None):
    return get_koji_allowlist(9)


@task
@init_settings("data")
@store_result("zuul_centos_distgit.json")
def zuul_centos_distgit(c, data=None):
    return get_zuul_centos_distgit()


@task(srpm_names, koji_allowlist_8, koji_allowlist_9)
@init_settings("data")
@store_result("gitlab_branches.json")
def gitlab_branches(c, data=None):
    packages = get_packages_from_json(data)
    return getAllBranches(
        packages, "https://gitlab.com/redhat/centos-stream/rpms/{name}.git"
    )


@task(srpm_names, module_pkg_names)
@init_settings("data")
@store_result("rpm_branches.json")
def rpm_branches(c, data=None):
    packages = get_packages_from_json(data)
    return getAllBranches(packages, "https://pkgs.devel.redhat.com/git/rpms/{name}")


def get_dates(cache):
    jsons = [p for p in Path(cache).iterdir() if p.as_posix().endswith(".utcnow.json")]
    data = {}
    for p in jsons:
        with open(p, encoding="utf-8") as f:
            data[p.name.split(".")[0]] = json.load(f)["utcnow"]

    return data


def print_table_to_console(header, table):
    table1 = Table(show_header=True, header_style="bold")
    for h in header:
        table1.add_column(h)
    for l in table:
        table1.add_row(*[str(x) for x in l])

    console = Console()
    console.print(table1)


def query(db, dir, name, description, query):
    print(query)
    cur = db.execute(query)
    lst = cur.fetchall()
    header = [h[0] for h in cur.description]

    print_table_to_console(header, lst)

    updated = db.execute("select min(utcnow) from updated").fetchall()[0]

    if len(header) == 1:
        with open(f"{dir}/{name}.json", "w", encoding="utf-8") as f:
            json.dump(
                {
                    "name": name,
                    "description": description,
                    "header": header,
                    "data": [l[0] for l in lst],
                    "type": "list",
                    "utcnow": updated,
                },
                f,
                ensure_ascii=False,
                indent=4,
            )
    else:
        with open(f"{dir}/{name}.json", "w", encoding="utf-8") as f:
            json.dump(
                {
                    "name": name,
                    "description": description,
                    "header": header,
                    "table": lst,
                    "type": "table",
                    "utcnow": updated,
                },
                f,
                ensure_ascii=False,
                indent=4,
            )


def import_json_table(db, name, data=None):
    header = data[0].keys()
    db.execute(f"drop table if exists {name}")
    db.execute(f"create table {name} ({', '.join(header)})")

    for row in data:
        db.execute(
            f"insert into {name} ({', '.join(header)}) values ({', '.join('?' for h in header)});",
            list(row.values()),
        )


def add_single_column_table(db, name, column):
    db.execute(f"drop table if exists {name}")
    db.execute(f"create table {name} (name)")
    for v in list(column):
        db.execute(f"insert into {name} (name) values (?);", [v])

    db.executescript(
        f"""
    create index i_{name} on {name} (name);
    """
    )


@task(
    gitlab_branches, koji_allowlist_8, koji_allowlist_9, srpm_names, zuul_centos_distgit
)
@init_settings("data")
def analyze_prepare_data(c, data=None):
    db = sqlite3.connect(str(data + "/db.sqlite"))

    # import_csv(db, "rpm_branches", f"{data}/rpm_branches.csv")
    # import_csv(db, "rpm_branches_fail", f"{data}/rpm_branches_fail.csv")

    tables = load_data(data + "/gitlab_branches.json")
    import_json_table(db, "gitlab_branches", tables["data"]["success"])
    import_json_table(db, "gitlab_branches_fail", tables["data"]["failure"])

    # create index i_r_bs on rpm_branches (package, branch);
    db.executescript(
        """
    create index i_g_bs on gitlab_branches (package, branch);
    """
    )

    add_single_column_table(
        db, "koji_allowlist_8", load_data(data + "/koji_allowlist_8.json")["data"]
    )
    add_single_column_table(
        db, "koji_allowlist_9", load_data(data + "/koji_allowlist_9.json")["data"]
    )

    add_single_column_table(
        db, "zuul_centos_distgit", load_data(data + "/zuul_centos_distgit.json")
    )

    dates = get_dates(data)
    db.execute(f"drop table if exists updated")
    db.execute(f"create table updated (name,utcnow)")
    for k, v in dates.items():
        db.execute(f"insert into updated (name,utnow) values (?, ?);", [k, v])

    c.run("mkdir -p analyses")


@task(analyze_prepare_data)
@init_settings("data", "analyses")
def analyze_checkout_failures(c, data=None, analyses=None):
    db = sqlite3.connect(str(data + "/db.sqlite"))
    # query(db, "rpm_fail","List of packages, where we couldn't download info about the rhel distgit repo","select distinct * from rpm_branches_fail")
    query(
        db,
        analyses,
        "gitlab_fail",
        "List of packages, where we couldn't download info about the centos stream gitlab repo",
        "select distinct * from gitlab_branches_fail",
    )


@task(analyze_prepare_data)
@init_settings("data", "analyses")
def analyze_centos_distgit(c, data=None, analyses=None):
    db = sqlite3.connect(str(data + "/db.sqlite"))

    template = """
      select distinct
          gb.package,
          EXISTS(select branch from gitlab_branches where package=gb.package and branch = "refs/heads/c{number}s") as c{number}s,
          EXISTS(select name from koji_allowlist_{number} where name=gb.package) as koji_allowlist_{number},
          EXISTS(select name from zuul_centos_distgit where name=gb.package) as zuul_centos_distgit
      from
        (select package from gitlab_branches UNION SELECT name as package from koji_allowlist_{number}) as gb
      where (koji_allowlist_{number} + c{number}s) = 1
      """

    query(
        db,
        analyses,
        "8_branches",
        "List of packages where there is either c8s branch or koji allowlist entry missing",
        template.format(number="8"),
    )
    query(
        db,
        analyses,
        "9_branches",
        "List of packages where there is either c8s branch or koji allowlist entry missing",
        template.format(number="9"),
    )


@task(analyze_prepare_data)
@init_settings("data", "analyses")
def analyze_centos_koji(c, data=None, analyses=None):
    db = sqlite3.connect(str(data + "/db.sqlite"))

    template = """
      select distinct
          gb.package
      from
        (select package from gitlab_branches UNION SELECT name as package from koji_allowlist_{number}) as gb
      where NOT EXISTS(select branch from gitlab_branches where package=gb.package and branch = "refs/heads/c{number}s")
      AND       EXISTS(select name from koji_allowlist_{number} where name=gb.package)
      AND   NOT EXISTS(select name from zuul_centos_distgit where name=gb.package)
      """

    query(
        db,
        analyses,
        "8_koji",
        "List of koji 8 allowlist entries witoug c8s branch an zuul config",
        template.format(number="8"),
    )
    query(
        db,
        analyses,
        "9_koji",
        "List of koji 9 allowlist entries witoug c8s branch an zuul config",
        template.format(number="9"),
    )


@task
@task(analyze_prepare_data)
@init_settings("data", "analyses")
def analyze_zull_and_koji(c, data=None, analyses=None):
    db = sqlite3.connect(str(data + "/db.sqlite"))

    template = """
      select distinct
          gb.package,
          EXISTS(select name from koji_allowlist_8 where name=gb.package) as k8,
          EXISTS(select name from koji_allowlist_9 where name=gb.package) as k9,
          EXISTS(select name from zuul_centos_distgit where name=gb.package) as zuul_centos_distgit
      from
        gitlab_branches as gb
      where (k8 + k9) = 1
      """

    query(
        db,
        analyses,
        "zuul_koji",
        "Presence of packages in relevant koji-ops tag-ansible config and zuul distgit config",
        template,
    )


@task(analyze_checkout_failures, analyze_centos_distgit, analyze_centos_koji)
@init_settings("data")
def render_tables(c, data=None):
    c.run("mkdir -p public")
    render_html.render_dir("analyses", "public")


"""
ns = Collection(
    clean,
    srpm_names,
    module_pkg_names,
    koji_allowlist_8,
    koji_allowlist_9,
    zuul_centos_distgit,
    render_tables,
    analyze_checkout_failures,
    analyze_centos_distgit,
    analyze_centos_koji,
    analyze_prepare_data
)
# ns.add_collection()
ns.configure({"settings": {"data": "./data", "analyses": "./analyses"}})
"""
