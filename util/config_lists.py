import urllib.request

import yaml

from util.pkg_names import load_data


def get_koji_allowlist(number):
    yaml_url = f"https://gitlab.com/redhat/centos-stream/release-engineering/koji-ops/-/raw/main/tag-ansible/roles/stream{str(number)}/vars/main.yml"

    with urllib.request.urlopen(yaml_url) as response:
        koji = response.read()

    d = yaml.safe_load(koji)
    set_koji = {p.replace("+", "plus") for p in d["allowlist_pkgs"]}
    return sorted(set_koji)


def get_zuul_centos_distgit():
    yaml_url = "https://gitlab.com/redhat/centos-stream/ci-cd/zuul/project-config/-/raw/master/resources/centos-distgits.yaml"

    with urllib.request.urlopen(yaml_url) as response:
        distgit_yaml = response.read()

    p = yaml.safe_load(distgit_yaml)
    set_zuul = {
        r.split("/")[-1]
        for d in p["resources"]["projects"]["centos-distgits"]["source-repositories"]
        for r in d.keys()
    }
    return sorted(set_zuul)


def get_packages_from_json(cache):
    srpm_names = load_data(cache + "/srpm_names.json")["data"]
    # modules_pkgs = load_data(cache+'/module_pkg_names.json')['data']
    modules_pkgs = []
    koji8pkg = set(load_data(cache + "/koji_allowlist_8.json")["data"])
    koji9pkg = set(load_data(cache + "/koji_allowlist_9.json")["data"])

    return sorted(
        koji8pkg.union(koji9pkg)
        .union({m.split("@")[0] for k in modules_pkgs for m in modules_pkgs[k]})
        .union({p for k in srpm_names for p in srpm_names[k]})
    )
