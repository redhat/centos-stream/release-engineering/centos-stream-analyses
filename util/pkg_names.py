# This code has been canibalised from A. Samalik's c8s-imports script
# https://gitlab.cee.redhat.com/asamalik/c8s-imports

import json
import os
import subprocess
import sys
import tempfile
import urllib.request

import yaml

###############################################################################
###  Cloning repos  ###########################################################
###############################################################################


def get_srpm_names(settings):
    rpms_json_urls = {
        "stream-latest": "https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/metadata/rpms.json",
    }

    # Step 1: Populate all non-modular repos

    log("Getting a list of all non-modular packages...")

    # a/ shipped
    srpm_names = {
        k: sorted(get_all_srpm_names(rpms_json_urls[k])) for k in rpms_json_urls
    }

    return srpm_names


def get_modules_pkgs(settings):
    modules_json_urls = {
        "stream-latest": "https://composes.stream.centos.org/production/latest-CentOS-Stream/compose/metadata/modules.json",
    }

    log("Getting a list of all modular packages... (this will take a while)")

    # a/ shipped

    def getModuleMds(json_url):
        module_name_streams = get_rhel_module_list(json_url, settings)
        with tempfile.TemporaryDirectory() as temp:
            settings["tmp"] = temp
            clone_module_repos(module_name_streams, "centpkg", settings)
            mds = get_old_rhel_modulemds(module_name_streams, settings)
        return mds

    modules_pkgs = {
        k: sorted(
            {
                f"{srpm_name}@{module_ns}"
                for module_ns, modulemd in getModuleMds(modules_json_urls[k]).items()
                if "components" in modulemd["data"]
                for srpm_name, srpm_data in modulemd["data"]["components"][
                    "rpms"
                ].items()
            }
        )
        for k in modules_json_urls
    }

    return modules_pkgs


def get_all_srpm_names(rpms_json_url):
    # Exclude list 1

    exclude_list = [
        "redhat-bookmarks",
        "redhat-indexhtml",
        "redhat-logos",
        "redhat-release",
        "cc-config",
        "module-build-macros",
        "redhat-indexhtml",
        "tree",  # This one is fine, has a different name in CentOS Stream because GitLab isn't happy with tree. So let's just exclude it for simplicity.
    ]

    all_srpm_names = set()

    with urllib.request.urlopen(rpms_json_url) as response:
        rpms_json_raw = response.read()
    rpms_json = json.loads(rpms_json_raw)

    for repo, arches in rpms_json["payload"]["rpms"].items():
        for arch, srpms in arches.items():
            for srpm_nevra in srpms.keys():
                # Exclude list 2

                if ".module+" in srpm_nevra:
                    continue

                if srpm_nevra.startswith("kpatch-patch-"):
                    continue

                if srpm_nevra.startswith("kmod-redhat-"):
                    continue

                srpm_name = pkg_id_to_name(srpm_nevra)

                if srpm_name in exclude_list:
                    continue

                all_srpm_names.add(srpm_name)

    return all_srpm_names


def get_rhel_module_list(modules_json_url, settings):
    module_name_streams = {}

    # Some manual additions not in the repos

    module_name_streams["javapackages-tools"] = set()
    module_name_streams["javapackages-tools"].add("202201")

    module_name_streams["javapackages-bootstrap"] = set()
    module_name_streams["javapackages-bootstrap"].add("202201")

    module_name_streams["perl-bootstrap"] = set()
    module_name_streams["perl-bootstrap"].add("5.30")
    module_name_streams["perl-bootstrap"].add("5.32")

    module_name_streams["perl-YAML-bootstrap"] = set()
    module_name_streams["perl-YAML-bootstrap"].add("1.24")

    module_name_streams["python38"] = set()
    module_name_streams["python38"].add("3.8-bootstrap")

    module_name_streams["python39"] = set()
    module_name_streams["python39"].add("3.9-bootstrap")

    # Get them from the repos

    with urllib.request.urlopen(modules_json_url) as response:
        modules_json_raw = response.read()
    modules_json = json.loads(modules_json_raw)

    for repo in modules_json["payload"]["modules"]:
        for arch in modules_json["payload"]["modules"][repo]:
            for nsvc, nsvc_data in modules_json["payload"]["modules"][repo][
                arch
            ].items():
                name = nsvc_data["metadata"]["name"]
                stream = nsvc_data["metadata"]["stream"]

                # Skip devel
                if name.endswith("-devel"):
                    continue

                # Skip perl:5.24
                if name == "perl" and stream == "5.24":
                    continue

                if name not in module_name_streams:
                    module_name_streams[name] = set()

                module_name_streams[name].add(stream)

    return module_name_streams


def clone_module_repos(module_name_streams, cmdpkg, settings):
    log("  Cloning module repos...")

    failed_clones = set()

    counter = 0
    directory = settings["tmp"]
    for module_name in module_name_streams.keys():
        counter += 1

        command = ["ls", module_name]

        result = subprocess.run(command, cwd=directory, capture_output=True, text=True)

        if result.returncode == 0:
            log(
                "    [{} of {}] {} (exists, skipping)".format(
                    counter, len(module_name_streams.keys()), module_name
                )
            )
            continue

        command = [cmdpkg, "clone", "-a", f"modules/{module_name}"]

        log(
            "    [{} of {}] {}".format(
                counter, len(module_name_streams.keys()), module_name
            )
        )
        result = subprocess.run(command, cwd=directory, capture_output=True, text=True)

        if result.returncode != 0:
            failed_clones.add(module_name)

    log("  Done!")

    if failed_clones:
        log("  Some clones failed:")
        for repo_name in sorted(failed_clones):
            log(f"    {repo_name}")

    log("")


def get_old_rhel_modulemds(module_name_streams, settings):
    log("  Loading modulemd files...")

    failed_checkouts = set()

    modulemds = {}
    for name, streams in module_name_streams.items():
        for stream in streams:
            try:
                branch = f"{stream}-rhel-8.8.0"

                # Fix some weirdly named branches
                if name == "pmdk" and stream == "1_fileformat_v6":
                    branch = "1-fileformat-v6-rhel-8.8.0"

                if name == "javapackages-tools" and stream == "201902":
                    branch = "201902"
                # End fixes

                command = ["git", "checkout", branch]
                directory = os.path.join(settings["tmp"], name)

                result = subprocess.run(
                    command, cwd=directory, capture_output=True, text=True
                )

                if result.returncode != 0:
                    failed_checkouts.add(f"{name}:{stream} ({branch})")
                    continue

                modulemd = load_yaml_data(os.path.join(directory, f"{name}.yaml"))

                modulemds[f"{name}:{stream}"] = modulemd
            except Exception as e:
                log(f"Skipping {name}:{stream} ({branch}) because of {str(e)}")
                failed_checkouts.add(f"{name}:{stream} ({branch})")

    log("  Done!")

    if failed_checkouts:
        log("  Some git checkouts failed:")
        for failed_checkout in sorted(failed_checkouts):
            log(f"    {failed_checkout}")

    log("")

    return modulemds


###############################################################################
###  Producing an output  #####################################################
###############################################################################


def initialise():
    settings = {
        "data_dir": os.path.join(os.getcwd(), "data"),
    }

    settings["data_file_full_path"] = os.path.join(
        settings["data_dir"], "all_pkgs_data.json"
    )
    settings["data_file_analysed_full_path"] = os.path.join(
        settings["data_dir"], "all_pkgs_data_analysed.json"
    )
    settings["component_ownership_repo"] = os.path.join(
        settings["data_dir"], "component_ownership_repo"
    )

    return settings


###############################################################################
###  Support functions  #######################################################
###############################################################################


def log(msg=""):
    print(msg, file=sys.stderr)


def pkg_id_to_name(pkg_id):
    pkg_name = pkg_id.rsplit("-", 2)[0]
    return pkg_name


class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return sorted(obj)
        if isinstance(obj, jinja2.Environment):
            return ""
        return json.JSONEncoder.default(self, obj)


def dump_data(path, data):
    with open(path, "w") as file:
        json.dump(data, file, cls=SetEncoder)


def load_data(path):
    with open(path) as file:
        data = json.load(file)
    return data


def load_yaml_data(path):
    with open(path) as file:
        data = yaml.safe_load(file)
    return data
