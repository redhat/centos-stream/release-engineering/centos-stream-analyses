import datetime
import json
from functools import wraps
from pathlib import Path


def init_settings(*cfg):
    def decorator(fn):
        @wraps(fn)
        def wrapTheFunction(console, **args):
            for entry in cfg:
                if entry not in args or args[entry] is None:
                    args[entry] = console.settings[entry]
            return fn(console, **args)

        return wrapTheFunction

    return decorator


def store_result(filename=None):
    def decorator(fn):
        @wraps(fn)
        def wrapTheFunction(c, **args):
            path = Path(f"{ args['data']}/{filename}")
            if filename is None:
                path = Path(f"{ args['data']}/{fn.__name__}.json")

            c.run(f"mkdir -p { args['data']}")
            if path.exists():
                print(f"{path} already exist, skipping")
                return

            result = fn(c, **args)
            with open(path, "w", encoding="utf-8") as f:
                json.dump(
                    {"data": result, "utcnow": datetime.datetime.utcnow().isoformat()},
                    f,
                    ensure_ascii=False,
                    indent=4,
                )

        return wrapTheFunction

    return decorator
