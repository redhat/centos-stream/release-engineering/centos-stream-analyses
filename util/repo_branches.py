import tempfile
import time
from multiprocessing import Pool, Value

import pygit2

counter = Value("i", 0)
start = time.time()


def getRemotes(x):
    name, url, number = x
    counter.value += 1
    if counter.value % 100 == 0:
        print(
            f"Processed {counter.value}/{number} of repos in {time.time()-start}",
            flush=True,
        )
    try:
        with tempfile.TemporaryDirectory() as tmpdirname:
            repo = pygit2.init_repository(tmpdirname, bare=True)
            repo.remotes.set_url("origin", url)
            return (
                name,
                [
                    {"name": b["name"], "oid": str(b["oid"])}
                    for b in repo.remotes["origin"].ls_remotes()
                ],
                "ok",
            )
    except Exception as e:
        return (name, [], str(e))


def getAllBranches(packages, template="https://pkgs.devel.redhat.com/git/rpms/{name}"):
    counter.value = 0
    number = len(packages)
    with Pool(50) as p:
        branches = p.map(
            getRemotes,
            [(name, template.format(name=name), number) for name in packages],
        )

    print("Finished getting branches")
    return {
        "success": [
            {"package": r, "branch": b["name"], "oid": b["oid"]}
            for r, s, e in branches
            if e == "ok"
            for b in s
        ],
        "failure": [{"package": r, "error": e} for r, s, e in branches if e != "ok"],
    }
