import json
import os

import bokeh
from bokeh.io import show
from bokeh.models import (
    Column,
    ColumnDataSource,
    DataTable,
    Div,
    HTMLTemplateFormatter,
    TableColumn,
    TabPanel,
    Tabs,
)


def red_fmt(my_col):
    template = """
        <div style="background:<%=
            (function colorfromint(){
                if(result_col == 'no' || result_col == 'False' || result_col == '0'){
                    return('#f14e08')}
                else{
                    return('#fff')}
                }()) %>;">
        <%= value %>
        </div>
    """.replace(
        "result_col", my_col
    )

    return HTMLTemplateFormatter(template=template)


def pivot_table_columns(header, table):
    columns = {h: [] for h in header}
    for l in table:
        keys = list(columns.keys())
        columns = {keys[i]: columns[keys[i]] + [v] for i, v in enumerate(l)}
    return columns


def render_tab(columns):
    columns["data"] = pivot_table_columns(columns["header"], columns["table"])
    b_columns = [
        TableColumn(field=c, title=c, formatter=red_fmt(c))
        for c in columns["data"].keys()
    ]
    table = DataTable(source=ColumnDataSource(columns["data"]), columns=b_columns)
    description = Div(
        text=f"<h3 style=\"text-align: center\">{columns['description']}</h3>"
    )
    link = Div(
        text=f"<a href='{columns['name']}.html'>Link to standalone {columns['name']} table</a>"
    )
    date = Div(text=f"Generated from data collected on {str(columns['utcnow'])}")
    column = Column(description, table, link, date)
    return column


def render_list(columns):
    description = Div(
        text=f"<h3 style=\"text-align: center\">{columns['description']}</h3>"
    )
    lst = [Div(text=f"<ul>{''.join('<li>'+p+'</li>' for p in columns['data'])}</ul>")]
    link = Div(
        text=f"<a href='{columns['name']}.html'>Link to standalone {columns['name']} table</a>"
    )
    date = Div(text=f"Generated from data collected on {str(columns['utcnow'])}")
    column = Column(description, link, *lst)
    return column


def render_columns(columns):
    if columns["type"] == "table":
        return render_tab(columns)
    elif columns["type"] == "list":
        return render_list(columns)


def render_file(file, out_dir):
    with open(f"{file}") as f:
        columns = json.load(f)
        bokeh.io.output_file(f"{out_dir}/{columns['name']}.html")
        show(render_columns(columns))
        return TabPanel(child=render_columns(columns), title=columns["name"])


def render_dir(in_dir, out_dir):
    title = Div(
        text='<h1 style="text-align: center">centos-stream-analyses tables</h1>'
    )
    files = [
        os.path.join(in_dir, f)
        for f in os.listdir(in_dir)
        if os.path.isfile(os.path.join(in_dir, f))
    ]
    tabs = [render_file(file, out_dir) for file in files]
    layout = Column(title, Tabs(tabs=tabs), sizing_mode="scale_width")

    bokeh.io.output_file(f"{out_dir}/index.html")
    show(layout)
