# Centos Stream Analyses

This repo should serve as a better home to various utility scripts,
that provide information about packages under CS care,
and provide a small static dashboard to quickly review the data:

https://redhat.gitlab.io/centos-stream/release-engineering/centos-stream-analyses/

## Usage

[Invoke](https://www.pyinvoke.org/) serves as the executor for the script. To run all of the tasks,
it should be enough to run `invoke render-tables` which, if there is no `data` directory present, will
run all of the tasks to produce a html table with [Bokeh](https://docs.bokeh.org/).

If there is relevant csv or json in `data` already it will only recompute the tables themselves.
To reset the data run `invoke clean`

## Extending

This utility has been written with iteration in mind, it is easy to create new tasks,
or re-run them.

You can list all of the tasks with `invoke --list`

There two main types of tasks, those that collect data and store it in the data dir,
and those that read data from the data directory, analyse it and store results in analyses dir.

Most of the analysis tasks utilise a sqlite db, that is initialised with the stored jsons.

There are two more tasks, `clean` that removes collected data and `render-tables` that takes the jsons of analyses and renders them as html tables and stores them in `./public` dir. This works well with publishing the results in gitlab pages.

## Formatting and style

I am using [shed](https://github.com/Zac-HD/shed) to source good configuration to standard
python formatting tools. Most of the heavy lifting is done by isort and black.
